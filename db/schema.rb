# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170122160543) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "albums", force: :cascade do |t|
    t.integer  "band_id"
    t.string   "name"
    t.integer  "year"
    t.string   "photo"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.datetime "deleted_at"
    t.string   "description"
    t.index ["band_id"], name: "index_albums_on_band_id", using: :btree
    t.index ["deleted_at"], name: "index_albums_on_deleted_at", using: :btree
  end

  create_table "bands", force: :cascade do |t|
    t.string   "name"
    t.string   "photo"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_bands_on_deleted_at", using: :btree
  end

  create_table "comments", force: :cascade do |t|
    t.text     "body"
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "events", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "location"
    t.datetime "time"
    t.string   "photo"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "fanclubs", force: :cascade do |t|
    t.string   "name"
    t.string   "location"
    t.string   "photo"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_fanclubs_on_deleted_at", using: :btree
  end

  create_table "fans", force: :cascade do |t|
    t.integer  "fanclub_id"
    t.string   "name"
    t.integer  "age"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.datetime "deleted_at"
    t.string   "photo"
    t.index ["deleted_at"], name: "index_fans_on_deleted_at", using: :btree
    t.index ["fanclub_id"], name: "index_fans_on_fanclub_id", using: :btree
  end

  create_table "members", force: :cascade do |t|
    t.integer  "band_id"
    t.string   "name"
    t.integer  "age"
    t.string   "instrument"
    t.text     "bio"
    t.string   "photo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.index ["band_id"], name: "index_members_on_band_id", using: :btree
    t.index ["deleted_at"], name: "index_members_on_deleted_at", using: :btree
  end

  create_table "songs", force: :cascade do |t|
    t.integer  "album_id"
    t.string   "name"
    t.string   "duration"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.integer  "number"
    t.index ["album_id"], name: "index_songs_on_album_id", using: :btree
    t.index ["deleted_at"], name: "index_songs_on_deleted_at", using: :btree
  end

  add_foreign_key "albums", "bands"
  add_foreign_key "fans", "fanclubs"
  add_foreign_key "members", "bands"
  add_foreign_key "songs", "albums"
end
