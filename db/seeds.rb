# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Band.delete_all
Fanclub.delete_all
Fan.delete_all
Album.delete_all
Song.delete_all
Member.delete_all
Event.delete_all
Comment.delete_all

band1 = Band.create(name: 'Red Hot Chili Peppers',
                    photo: 'https://consequenceofsound.files.wordpress.com/2016/05/red-hot-chili-peppers-dark-necessi'\
                    'ties-single-new-mp3.png',
                    description: 'Red Hot Chili Peppers, also sometimes shortened to "The Chili Peppers" or '\
                    'abbreviated as "RHCP", are an American funk rock band formed in Los Angeles in 1983. '\
                    'The group\'s '\
                    'musical style primarily consists of rock with an emphasis on funk, as well as elements from '\
                    'other genres such as punk rock and psychedelic rock. When played live, their music incorporates '\
                    'elements of jam band due to the improvised nature of much of their performances. '\
                    'Currently, the band consists of founding members vocalist Anthony Kiedis and bassist Flea, '\
                    'longtime drummer Chad Smith, and former touring guitarist Josh Klinghoffer. Red Hot Chili '\
                    'Peppers are one of the best-selling bands of all time with over 80 million records sold '\
                    'worldwide, have '\
                    'been nominated for sixteen Grammy Awards, which they have won six, and are the most successful '\
                    'band in alternative rock radio history, currently holding the records for most number-one '\
                    'singles, most cumulative weeks at number one and most top-ten songs on the Billboard Alternative'\
                    ' Songs chart. In 2012, they were inducted into the Rock and Roll Hall of Fame.')
Band.create(name: 'Led Zeppelin',
            photo: 'https://s-media-cache-ak0.pinimg.com/736x/18/44/d2/1844d2c6550ec6e2fc1aa31acf3576db.jpg',
            description: 'A really awesome band as well.')
Band.create(name: 'Black Sabbath',
            photo: 'http://www.phantomsway.com/wp-content/uploads/2015/12/BlackSabbath.jpg',
            description: 'Another really awesome band.')
Band.create(name: 'Simon & Garfunkel',
            photo: 'http://www.noise11.com/wp/wp-content/uploads/2015/07/Simon-and-Garfunkel.jpg',
            description: 'Also really good band.')

Fanclub.create(name: 'RHCP fanclub', location: 'California',
                          photo: 'https://pbs.twimg.com/profile_images/577934997652267009/RFcZvug7.jpeg',
                          description: 'Some info about us.')
Fanclub.create(name: 'Black Sabbathists', location: 'Alaska',
                          photo: 'http://img.wennermedia.com/social/rs-225449-94171748_.jpg',
                          description: 'Some info about us.')

album1 = Album.create(name: 'The Getaway', year: '2016',
                      photo: 'http://www.clashmusic.com/sites/default/files/field/image/red-hot-chili-peppers-the-get'\
                      'away-ltd.jpg',
                      description: 'The Getaway is the eleventh studio album by American rock band Red Hot Chili '\
                      'Peppers, released through Warner Bros. on June 17, 2016. This is the band\'s first studio '\
                      'album since 2011\'s I\'m with You. It was produced by Danger Mouse, who replaced Rick Rubin '\
                      'after twenty-five years and six albums as the band\'s producer, making it the first non-Rubin '\
                      'produced album since 1989\'s Mother\'s Milk, while its release makes the current lineup of the'\
                      ' band only the second in the group\'s history to record more than one album together.',
                      band_id: band1.id)
Album.create(name: 'I\'m with you', year: '2011',
                      photo: 'http://myglobalmind.com/wp-content/uploads/2011/08/Red-Hot-Chili-Peppers-Im-With-You.jpg',
                      description: 'I\'m with You is the tenth studio album by the American rock band Red Hot Chili '\
                      'Peppers. The album was released by Warner Bros. Records on August 30, 2011. The album made its'\
                      ' debut at number one in eighteen different countries including the United Kingdom while '\
                      'reaching number two in the United States and Canada.', band: band1)
Album.create(name: 'Stadium Arcadium', year: '2006',
                      photo: 'https://www.musicdirect.com/Portals/0/Hotcakes/Data/products/d156bf15-6caf-48f0-a690-fa'\
                      'bfe8e1edb8/medium/LDR39110.jpg',
                      description: 'Stadium Arcadium is the ninth studio album by American rock band Red Hot Chili '\
                      'Peppers. The album was released on May 9, 2006, on Warner Bros. Records. The album produced '\
                      'five singles: "Dani California", "Tell Me Baby", "Snow (Hey Oh)", "Desecration Smile", and '\
                      '"Hump de Bump" along with the first ever fan made music video for the song, "Charlie". '\
                      'In the U.S., Stadium Arcadium became the band\'s first number one selling album. '\
                      'According to the band\'s vocalist Anthony Kiedis, Stadium Arcadium was originally scheduled '\
                      'to be a trilogy of albums each released six months apart, but was eventually condensed into '\
                      'a double album. The album is also the group\'s last to feature guitarist John Frusciante, '\
                      'who confirmed his departure from the band in 2009.', band: band1)

Member.create(name: 'Anthony Kiedis', age: 1962, instrument: 'singer',
              photo: 'http://www.alternativenation.net/wp-content/uploads/2016/05/anthonykiedishat.jpg',
              bio: 'Anthony Kiedis is an American musician best known as the lead singer and lyricist of the band '\
              'Red Hot Chili Peppers, which he has fronted since its conception in 1983.', band_id: band1.id)
Member.create(name: 'Flea', age: 1962, instrument: 'bass',
              photo: 'https://artists4bernie.files.wordpress.com/2016/02/rhcp-flea-loves-legendary-singer-percy-sledg'\
              'e-fdrmx.jpg?w=1200',
              bio: 'Michael Peter Balzary, better known by his stage name Flea, is an Australian-American musician, '\
              'best known as a co-founding member and one of the composers of the rock band Red Hot Chili Peppers '\
              'with whom he was inducted in 2012 into the Rock and Roll Hall of Fame.', band_id: band1.id)
Member.create(name: 'Chad Smith', age: 1961, instrument: 'drums',
              photo: 'http://www.bottlerocknapavalley.com/wp-content/uploads/2015/04/chad-smith-billboard-interview-m'\
              'arch-6th-2012-main.jpg',
              bio: 'Chadwick "Chad" Gaylord Smith is an American musician and the current drummer of the Red Hot '\
              'Chili Peppers, which he joined in 1988.', band_id: band1.id)
Member.create(name: 'Josh Klinghoffer', age: 1979, instrument: 'guitar',
              photo: 'http://www.fendercustomshop.com/custom-life/wp-content/uploads/assets/Josh2.jpg',
              bio: 'Josh Adam "Josh" Klinghoffer is an American musician best known as the current guitarist for '\
              'the rock band Red Hot Chili Peppers, with whom he has recorded two studio albums, I\'m with You (2011)'\
              ' and The Getaway (2016), and the b-sides compilation, I\'m Beside You (2013). Klinghoffer replaced his'\
              ' friend and frequent collaborator John Frusciante, in 2009, after a period of being a touring member.',
              band_id: band1.id)

Song.create(name: 'The Getaway', number: 1, duration: '4:10', album_id: album1.id)
Song.create(name: 'Dark Necessities', number: 2, duration: '5:02', album_id: album1.id)
Song.create(name: 'We Turn Red', number: 3, duration: '3:20', album_id: album1.id)
Song.create(name: 'The Longest Wave', number: 4, duration: '3:31', album_id: album1.id)
Song.create(name: 'Goodbye Angels', number: 5, duration: '4:29', album_id: album1.id)
Song.create(name: 'Sick Love', number: 6, duration: '3:41', album_id: album1.id)
Song.create(name: 'Go Robot', number: 7, duration: '4:24', album_id: album1.id)
Song.create(name: 'Feasting on the Flowers', number: 8, duration: '3:23', album_id: album1.id)
Song.create(name: 'Detroit', number: 9, duration: '3:47', album_id: album1.id)
Song.create(name: 'This Ticonderoga', number: 10, duration: '3:35', album_id: album1.id)
Song.create(name: 'Encore', number: 11, duration: '4:11', album_id: album1.id)
Song.create(name: 'The Hunter', number: 12, duration: '4:00', album_id: album1.id)
Song.create(name: 'Dreams of a Samurai', number: 13, duration: '6:09', album_id: album1.id)

event = Event.create(name: 'RHCP - Live in Prague', location: 'Prague', time: '2012-08-27 20:00:00',
                    photo: 'http://www.livedownloads.com/labels/RHCP120827.jpg',
                    description: 'I\'m with You is the tenth studio album by the American rock band Red Hot Chili '\
                    'Peppers. The album was released by Warner Bros. Records on August 30, 2011. The album made its '\
                    'debut at number one in eighteen different countries including the United Kingdom while reaching '\
                    'number two in the United States and Canada.')

comment1 = Comment.create(body: 'This will be amazing!', created_at: '2012-08-25 19:21:17',
                          commentable_type: 'Event', commentable_id: event.id)
Comment.create(body: 'I am definitely going', created_at: '2012-08-25 20:09:11',
                          commentable_type: 'Comment', commentable_id: comment1.id)
