class AddDeletedAtToFans < ActiveRecord::Migration[5.0]
  def change
    add_column :fans, :deleted_at, :datetime
    add_index :fans, :deleted_at
  end
end
