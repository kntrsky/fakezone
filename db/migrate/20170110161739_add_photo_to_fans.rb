class AddPhotoToFans < ActiveRecord::Migration[5.0]
  def change
    add_column :fans, :photo, :string
  end
end
