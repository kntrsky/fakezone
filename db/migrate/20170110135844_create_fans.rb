class CreateFans < ActiveRecord::Migration[5.0]
  def change
    create_table :fans do |t|
      t.references :fanclub, foreign_key: true
      t.string :name
      t.integer :age
      t.text :description

      t.timestamps
    end
  end
end
