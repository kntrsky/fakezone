class AddDescriptionToAlbum < ActiveRecord::Migration[5.0]
  def change
    add_column :albums, :description, :string
  end
end
