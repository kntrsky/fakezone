class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.string :name
      t.text :description
      t.string :location
      t.date :date
      t.datetime :time
      t.string :photo

      t.timestamps
    end
  end
end
