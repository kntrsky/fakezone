class AddNumberToSongs < ActiveRecord::Migration[5.0]
  def change
    add_column :songs, :number, :integer
  end
end
