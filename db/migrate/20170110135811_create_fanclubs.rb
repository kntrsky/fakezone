class CreateFanclubs < ActiveRecord::Migration[5.0]
  def change
    create_table :fanclubs do |t|
      t.string :name
      t.string :location
      t.string :photo
      t.text :description

      t.timestamps
    end
  end
end
