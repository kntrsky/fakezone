class CreateAlbums < ActiveRecord::Migration[5.0]
  def change
    create_table :albums do |t|
      t.references :band, foreign_key: true
      t.string :name
      t.integer :year
      t.string :photo

      t.timestamps
    end
  end
end
