class RemoveGenreFromBands < ActiveRecord::Migration[5.0]
  def change
    remove_column :bands, :genre, :string
  end
end
