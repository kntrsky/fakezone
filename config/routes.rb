Rails.application.routes.draw do
  resources :events do
    resources :comments
  end

  resources :comments do
    resources :comments
  end

  resources :fanclubs do
    resources :comments
    resources :fans
  end
  # resources :songs

  resources :bands do
    resources :comments
    resources :members
    resources :albums do
      resources :songs
    end
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'bands#index'
end
