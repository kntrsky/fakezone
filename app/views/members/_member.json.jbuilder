json.extract! member, :id, :name, :age, :instrument, :bio, :photo, :created_at, :updated_at
json.url member_url(member, format: :json)