json.extract! fan, :id, :name, :age, :description, :created_at, :updated_at
json.url fan_url(fan, format: :json)