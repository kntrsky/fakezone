json.extract! fanclub, :id, :name, :location, :photo, :description, :created_at, :updated_at
json.url fanclub_url(fanclub, format: :json)