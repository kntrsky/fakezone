json.extract! band, :id, :name, :photo, :description, :created_at, :updated_at
json.url band_url(band, format: :json)
