json.extract! album, :id, :name, :year, :photo, :created_at, :updated_at
json.url album_url(album, format: :json)