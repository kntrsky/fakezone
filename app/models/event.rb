class Event < ApplicationRecord
  default_scope { order(time: :asc) }

  has_many :comments, as: :commentable

  validates :name, presence: true, uniqueness: true
  validates_presence_of :time, :location
end
