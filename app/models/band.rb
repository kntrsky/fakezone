class Band < ApplicationRecord
  default_scope { order(name: :asc) }

  has_many :members, dependent: :destroy
  has_many :albums, dependent: :destroy

  has_many :comments, as: :commentable

  validates :name, presence: true, uniqueness: true
end
