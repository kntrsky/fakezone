class Album < ApplicationRecord
  belongs_to :band
  has_many :songs, dependent: :destroy
  default_scope { order(year: :desc) }

  validates_presence_of :band_id
  validates :name, presence: true, uniqueness: true
end
