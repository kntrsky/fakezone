class Fanclub < ApplicationRecord
  default_scope { order(name: :asc) }

  has_many :fans, dependent: :destroy

  has_many :comments, as: :commentable

  validates :name, presence: true, uniqueness: true
end
