class Member < ApplicationRecord
  belongs_to :band
  default_scope { order(name: :asc) }

  validates_presence_of :band_id, :age
  validates :name, presence: true, uniqueness: true
end
