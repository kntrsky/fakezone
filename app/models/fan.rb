class Fan < ApplicationRecord
  belongs_to :fanclub
  default_scope { order(name: :asc) }

  validates_presence_of :fanclub_id, :age
  validates :name, presence: true, uniqueness: true
end
