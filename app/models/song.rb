class Song < ApplicationRecord
  belongs_to :album
  default_scope { order(number: :asc) }

  validates_presence_of :album_id
  validates :name, presence: true, uniqueness: true
end
