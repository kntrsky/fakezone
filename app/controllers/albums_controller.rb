class AlbumsController < ApplicationController
  before_action :set_album, only: [:show, :edit, :update, :destroy]

  # GET /albums
  # GET /albums.json
  def index
    @albums = Album.all
  end

  # GET /albums/1
  # GET /albums/1.json
  def show
  end

  # GET /albums/new
  def new
    @band = Band.find(params[:band_id])
    @album = Album.new
  end

  # GET /albums/1/edit
  def edit
    @band = Band.find(params[:band_id])
    @album = @band.albums.find(params[:id])
  end

  # POST /albums
  # POST /albums.json
  def create
    @band = Band.find(params[:band_id])
    @album = @band.albums.new(album_params)

    if @album.save
      redirect_to band_path(@band)
    else
      render 'new'
    end
  end

  # PATCH/PUT /albums/1
  # PATCH/PUT /albums/1.json
  def update
    @band = Band.find(params[:band_id])
    @album = @band.albums.find(params[:id])

    if @album.update(album_params)
      redirect_to band_path(@band)
    else
      render 'edit'
    end
  end

  # DELETE /albums/1
  # DELETE /albums/1.json
  def destroy
    @band = Band.find(params[:band_id])
    @album = @band.albums.find(params[:id]).destroy
    redirect_to band_path(@band)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_album
    @band = Band.find(params[:band_id])
    @album = @band.albums.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def album_params
    params.require(:album).permit(:name, :year, :photo, :description)
  end
end
