class SongsController < ApplicationController
  # before_action :set_song, only: [:show, :edit, :update, :destroy]
  before_action :set_band_and_album, only: [:show, :new, :edit, :create, :update, :destroy]

  # GET /songs
  # GET /songs.json
  def index
    @songs = Song.all
  end

  # GET /songs/1
  # GET /songs/1.json
  def show
    # @band = Band.find(params[:band_id])
    # @album = band.albums.find(params[:band_id])
  end

  # GET /songs/new
  def new
    # @band = Band.find(params[:band_id])
    # @album = @band.albums.find(params[:album_id])
    @song = Song.new
  end

  # GET /songs/1/edit
  def edit
    # @band = Band.find(params[:band_id])
    # @album = @band.albums.find(params[:album_id])
    @song = @album.songs.find(params[:id])
  end

  # POST /songs
  # POST /songs.json
  def create
    # @band = Band.find(params[:band_id])
    # @album = @band.albums.find(params[:album_id])
    @song = @album.songs.new(song_params)

    if @song.save
      redirect_to band_album_path(@band, @album)
    else
      render 'new'
    end
  end

  # PATCH/PUT /songs/1
  # PATCH/PUT /songs/1.json
  def update
    # @band = Band.find(params[:band_id])
    # @album = @band.albums.find(params[:album_id])
    @song = @album.songs.find(params[:id])
    if @song.update(song_params)
      redirect_to band_album_path(@band, @album)
    else
      render 'edit'
    end
  end

  # DELETE /songs/1
  # DELETE /songs/1.json
  def destroy
    # @band = Band.find(params[:band_id])
    # @album = @band.albums.find(params[:album_id])
    @song = @album.songs.find(params[:id]).destroy
    redirect_to band_album_path(@band, @album)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_band_and_album
    @band = Band.find(params[:band_id])
    @album = @band.albums.find(params[:album_id])
  end

  # def set_song
  #  @song = Song.find(params[:id])
  # end

  # Never trust parameters from the scary internet, only allow the white list through.
  def song_params
    params.require(:song).permit(:number, :name, :duration)
  end
end
