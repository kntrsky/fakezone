class CommentsController < ApplicationController
  before_action :find_commentable

  def new
    @comment = Comment.new
  end

  def create
    @comment = @commentable.comments.new comment_params

    if @comment.save
      redirect_to :back, notice: 'Your comment was successfully posted!'
    else
      redirect_to :back, notice: "Your comment wasn't posted!"
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:body)
  end

  def find_commentable
    @commentable = Comment.find_by_id(params[:comment_id]) if params[:comment_id]
    @commentable = Event.find_by_id(params[:event_id]) if params[:event_id]
    @commentable = Band.find_by_id(params[:band_id]) if params[:band_id]
    @commentable = Fanclub.find_by_id(params[:fanclub_id]) if params[:fanclub_id]
  end
end
