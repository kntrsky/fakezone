class MembersController < ApplicationController
  before_action :set_member, only: [:show, :edit, :update, :destroy]

  # GET /members
  # GET /members.json
  def index
    @members = Member.all
  end

  # GET /members/1
  # GET /members/1.json
  def show
    @band = Band.find(params[:band_id])
  end

  # GET /members/new
  def new
    @band = Band.find(params[:band_id])
    @member = Member.new
  end

  # GET /members/1/edit
  def edit
    @band = Band.find(params[:band_id])
    @member = @band.members.find(params[:id])
  end

  # POST /members
  # POST /members.json
  def create
    @band = Band.find(params[:band_id])
    @member = @band.members.new(member_params)

    if @member.save
      redirect_to band_path(@band)
    else
      render 'new'
    end
  end

  # PATCH/PUT /members/1
  # PATCH/PUT /members/1.json
  def update
    @band = Band.find(params[:band_id])
    @member = @band.members.find(params[:id])

    if @member.update(member_params)
      redirect_to band_path(@band)
    else
      render 'edit'
    end
  end

  # DELETE /members/1
  # DELETE /members/1.json
  def destroy
    @band = Band.find(params[:band_id])
    @mmber = @band.members.find(params[:id]).really_destroy!
    redirect_to band_path(@band)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_member
    @member = Member.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def member_params
    params.require(:member).permit(:name, :age, :instrument, :bio, :photo)
  end
end
