class FansController < ApplicationController
  before_action :set_fan, only: [:show, :edit, :update, :destroy]

  # GET /fans
  # GET /fans.json
  def index
    @fans = Fan.all
  end

  # GET /fans/1
  # GET /fans/1.json
  def show
    @fanclub = Fanclub.find(params[:fanclub_id])
  end

  # GET /fans/new
  def new
    @fanclub = Fanclub.find(params[:fanclub_id])
    @fan = Fan.new
  end

  # GET /fans/1/edit
  def edit
    @fanclub = Fanclub.find(params[:fanclub_id])
    @fan = @fanclub.fans.find(params[:id])
  end

  # POST /fans
  # POST /fans.json
  def create
    @fanclub = Fanclub.find(params[:fanclub_id])
    @fan = @fanclub.fans.new(fan_params)
    if @fan.save
      redirect_to fanclub_path(@fanclub)
    else
      render 'new'
    end
  end

  # PATCH/PUT /fans/1
  # PATCH/PUT /fans/1.json
  def update
    @fanclub = Fanclub.find(params[:fanclub_id])
    @fan = @fanclub.fans.find(params[:id])

    if @fan.update(fan_params)
      redirect_to fanclub_path(@fanclub)
    else
      render 'edit'
    end
  end

  # DELETE /fans/1
  # DELETE /fans/1.json
  def destroy
    @fanclub = Fanclub.find(params[:fanclub_id])
    @fan = @fanclub.fans.find(params[:id]).destroy
    redirect_to fanclub_path(@fanclub)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_fan
    @fan = Fan.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def fan_params
    params.require(:fan).permit(:name, :age, :photo, :description)
  end
end
