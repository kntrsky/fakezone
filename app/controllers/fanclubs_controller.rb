class FanclubsController < ApplicationController
  before_action :set_fanclub, only: [:show, :edit, :update, :destroy]

  # GET /fanclubs
  # GET /fanclubs.json
  def index
    @fanclubs = Fanclub.all
  end

  # GET /fanclubs/1
  # GET /fanclubs/1.json
  def show
  end

  # GET /fanclubs/new
  def new
    @fanclub = Fanclub.new
  end

  # GET /fanclubs/1/edit
  def edit
  end

  # POST /fanclubs
  # POST /fanclubs.json
  def create
    @fanclub = Fanclub.new(fanclub_params)

    respond_to do |format|
      if @fanclub.save
        format.html { redirect_to @fanclub, notice: 'Fanclub was successfully created.' }
        format.json { render :show, status: :created, location: @fanclub }
      else
        format.html { render :new }
        format.json { render json: @fanclub.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fanclubs/1
  # PATCH/PUT /fanclubs/1.json
  def update
    respond_to do |format|
      if @fanclub.update(fanclub_params)
        format.html { redirect_to @fanclub, notice: 'Fanclub was successfully updated.' }
        format.json { render :show, status: :ok, location: @fanclub }
      else
        format.html { render :edit }
        format.json { render json: @fanclub.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fanclubs/1
  # DELETE /fanclubs/1.json
  def destroy
    @fanclub.destroy
    respond_to do |format|
      format.html { redirect_to fanclubs_url, notice: 'Fanclub was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_fanclub
    @fanclub = Fanclub.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def fanclub_params
    params.require(:fanclub).permit(:name, :location, :photo, :description)
  end
end
