require 'test_helper'

class FanclubsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @fanclub = fanclubs(:one)
  end

  test "should get index" do
    get fanclubs_url
    assert_response :success
  end

  test "should get new" do
    get new_fanclub_url
    assert_response :success
  end

  test "should create fanclub" do
    assert_difference('Fanclub.count') do
      post fanclubs_url, params: { fanclub: { description: @fanclub.description, location: @fanclub.location,
                                              name: @fanclub.name, photo: @fanclub.photo } }
    end

    assert_redirected_to fanclub_url(Fanclub.last)
  end

  test "should show fanclub" do
    get fanclub_url(@fanclub)
    assert_response :success
  end

  test "should get edit" do
    get edit_fanclub_url(@fanclub)
    assert_response :success
  end

  test "should update fanclub" do
    patch fanclub_url(@fanclub), params: { fanclub: { description: @fanclub.description, location: @fanclub.location,
                                                      name: @fanclub.name, photo: @fanclub.photo } }
    assert_redirected_to fanclub_url(@fanclub)
  end

  test "should destroy fanclub" do
    assert_difference('Fanclub.count', -1) do
      delete fanclub_url(@fanclub)
    end

    assert_redirected_to fanclubs_url
  end
end
